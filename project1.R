data = read.csv("household_power_consumption_subset.txt", sep=";", na.strings="?")

data$datetime = strptime(paste(data$Date, data$Time), "%d/%m/%Y %H:%M:%S")
head(data)

# Set locale to english
Sys.setlocale("LC_TIME", "English")
