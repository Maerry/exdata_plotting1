source("project1.R")

png(filename="plot4.png")

par(mfrow=c(2, 2))

# top left plot (plot 2)
with(data, plot(datetime, Global_active_power, xlab="", ylab="Global Active Power", type="l"))

# top right plot
with(data, plot(datetime, Voltage, type="l"))

# bottom left plot (plot 3)
columns = 7:9
plot_colors = c("black", "red", "blue")
plot(data$datetime,  data$Sub_metering_1, type="n", xlab="", ylab="Energy sub metering")
for (i in columns) {
  lines(data$datetime, data[, i], col=plot_colors[i-6])
}
legend("topright", lty=1, col=plot_colors, legend=names(data)[columns], bty="n")

# bottom right plot
with(data, plot(datetime, Global_reactive_power, type="l"))

dev.off()
